import 'package:flutter_app/DataGlobal/device.dart';
import 'device_state.dart';

class DataState extends DeviceState {
  final List<Device> deviceList;

  DataState({required this.deviceList});

  @override
  List<Object> get props => [deviceList];
}

