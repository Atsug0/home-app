import 'package:equatable/equatable.dart';

abstract class DeviceState extends Equatable {}

class InitialState extends DeviceState {
  @override
  List<Object> get props => [];
}