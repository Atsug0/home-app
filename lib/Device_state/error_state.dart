import 'device_state.dart';

class ErrorState extends DeviceState {
  final String error;
  ErrorState({required this.error});

  @override
  List<Object> get props => [error];
}
