import 'dart:convert';
import 'package:flutter_app/DataGlobal/data.dart';
import 'package:flutter_app/DataGlobal/device.dart';
import 'package:http/http.dart';

abstract class GetParse {
  Future<List<Device>> getDevice();
}

class GetParsed implements GetParse {
  @override
  Future<List<Device>> getDevice() async {
    var response = await get(Uri.parse('http://storage42.com/modulotest/data.json'));
    if (response.statusCode == 200) {
      List<Device>? deviceList =
          Data.fromJson(json.decode(response.body)).devices;
      if (deviceList != null) {
        return deviceList;
      }
    } else {
      throw Exception();
    }
    return [];
  }
}
