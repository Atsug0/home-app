import 'package:flutter/material.dart';
import 'package:flutter_app/DataGlobal/device.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:lite_rolling_switch/lite_rolling_switch.dart';

class ScreenSettingsShutter extends StatefulWidget {
  final Device device;
  const ScreenSettingsShutter({super.key, required this.device});

  @override
  State<ScreenSettingsShutter> createState() => _ScreenSettingsShutter(device);
}

class _ScreenSettingsShutter extends State<ScreenSettingsShutter> {
  Device device;
  _ScreenSettingsShutter(this.device);
  TextEditingController valueTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    int val = device.getValue();
      return Scaffold(
          appBar: AppBar(
              title: Text(device.getName()),
          ),
          body: Column(
            children: [
              Text(
                '$val'
              ),
              Slider(
                  value: device.getValue().toDouble(),
                  min:0,
                  max:100,
                  onChanged: ((value) {
                    setState(() {
                      device.setValue(value.toInt());
                      if (value > 0) {
                        device.setMode("ON");
                      } else {
                        device.setMode("OFF");
                      }
                      val = value.toInt();
                    });
                  })
              ),
            ],
          )
      );
    } 
}
