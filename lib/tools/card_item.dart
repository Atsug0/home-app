import 'package:flutter/material.dart';
import 'package:flutter_app/DataGlobal/device.dart';
import 'package:flutter_app/tools/modify_device.dart';
import 'package:flutter_app/tools/modify_device2.dart';


class CardItem extends StatefulWidget {
  final Device device;
  const CardItem({super.key, required this.device});

  @override
  State<CardItem> createState() => _CardItem(device: device);
}
class _CardItem extends State<CardItem> {
  final Device device;
  _CardItem(
      {required this.device,
      });

  @override
  Widget build(BuildContext context) {
    return Card(
        child: ListTile(
      onTap: () async {
        
        switch (device.getProd()) {
          case "RollerShutter":
            Navigator.push(
              context,
              MaterialPageRoute<void>(
                builder: (BuildContext context) =>
                    ScreenSettingsShutter(device: device),
              ),
            );
            
            break;
          default:
            Navigator.push(
              context,
              MaterialPageRoute<void>(
                builder: (BuildContext context) =>
                    ScreenSettingsOthers(device: device),
              ),
            ).then((context) => setState(() {
              
            },));
           
            break;
        }
      },
      title: Text(device.getName()),
      subtitle: Text(device.getState()),
    ));
  }
}
