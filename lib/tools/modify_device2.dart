import 'package:flutter/material.dart';
import 'package:flutter_app/DataGlobal/device.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:lite_rolling_switch/lite_rolling_switch.dart';

class ScreenSettingsOthers extends StatefulWidget {
  final Device device;
  const ScreenSettingsOthers({super.key, required this.device});

  @override
  State<ScreenSettingsOthers> createState() => _ScreenSettingsOthers(device);
}

class _ScreenSettingsOthers extends State<ScreenSettingsOthers> {
  Device device;
  _ScreenSettingsOthers(this.device);
  TextEditingController valueTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    int val = device.getValue();
      return Scaffold(
          appBar: AppBar(
              title: Text(device.getName()),
          ),
          body: Column(
            children: [
              Text(
                '$val'
              ),
              Slider(
                  value: device.getValue().toDouble(),
                  min: device.getProd() == "Heater" ? 5 : 0,
                  max:  device.getProd() == "Heater" ? 28 : 100,
                  onChanged: ((value) {
                    setState(() {
                      device.setValue(value.toInt());
                      if (value > 0) {
                        device.setMode("ON");
                      } else {
                        device.setMode("OFF");
                      }
                      val = value.toInt();
                    });
                  })
              ),
              LiteRollingSwitch(
                        value: device.getMode() == "ON",
                        textOn: "On",
                        textOff: "Off",
                        colorOn: Colors.greenAccent,
                        colorOff: Colors.redAccent,
                        iconOn: Icons.done,
                        iconOff: Icons.light_mode,
                        textSize: 18.0,
                        onChanged: (bool position) {
                          if (position) {
                            device.setMode("ON");
                            if (val == 0) {
                              val = 100;
                            }
                          } else {
                            device.setMode("OFF");
                            val = 0;
                          }
                        },
                        onDoubleTap: () {},
                        onSwipe: () {},
                        onTap: () {},
                      
              ),
            ],
          )
      );
    } 
}
