// ignore: file_names
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_app/Device_bloc/exported_bloc_device.dart';
import 'package:flutter_app/Device_state/exported_state.dart';
import 'package:flutter_app/DataGlobal/device.dart';
import 'package:flutter_app/tools/spinner.dart';
import 'package:flutter_app/tools/card_item.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  late DeviceBloc deviceBloc;
  @override
  void initState() {
    super.initState();
    deviceBloc = BlocProvider.of<DeviceBloc>(context);
    deviceBloc.add(DeviceEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return Material(
          child: Scaffold(
        appBar: AppBar(
          title: const Text('HomeApp'),
        ),
        body: BlocBuilder<DeviceBloc, DeviceState>(builder: ((context, state) {
          if (state is InitialState) {
            return const LoadingState();
          }
          if (state is DataState) {
            List<Device> deviceList = state.deviceList;
            return SafeArea(
              child: ListView.builder(
                itemCount: deviceList.length,
                itemBuilder: ((context, index) {
                  return CardItem(
                    device: deviceList[index],
                  );
                }),
              ),
            );
          }
          if (state is ErrorState) {
            return const LoadingState();
          }
          return const Scaffold();
        })),
      ));
    });
  }
}
