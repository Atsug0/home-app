// ignore: file_names
class User {
  String firstName;
  String lastName;
  Address address;
  int birthDate;
  
  User({
    required this.firstName,
    required this.lastName,
    required this.address,
    required this.birthDate,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        firstName: json["firstName"],
        lastName: json["lastName"],
        address: Address.fromJson(json["address"]),
        birthDate: json["birthDate"],
      );

  Map<String, dynamic> toJson() => {
        "firstName": firstName,
        "lastName": lastName,
        "address": address.toJson(),
        "birthDate": birthDate,
      };
}

class Address {
  Address({
    required this.city,
    required this.postalCode,
    required this.street,
    required this.streetCode,
    required this.country,
  });

  String city;
  int postalCode;
  String street;
  String streetCode;
  String country;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        city: json["city"],
        postalCode: json["postalCode"],
        street: json["street"],
        streetCode: json["streetCode"],
        country: json["country"],
      );

  Map<String, dynamic> toJson() => {
        "city": city,
        "postalCode": postalCode,
        "street": street,
        "streetCode": streetCode,
        "country": country,
      };
}