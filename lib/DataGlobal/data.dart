// To parse this JSON data, do
//
//     final data = dataFromJson(jsonString);
import 'device.dart';
import 'user.dart';
import 'dart:convert';

Data dataFromJson(String str) => Data.fromJson(json.decode(str));


class Data {
  List<Device>? devices;
  User? user;

  Data({
    this.devices,
    this.user,
  });

  Data.fromJson(Map<String, dynamic> json) {
    if (json['devices'] != null) {
      devices = <Device>[];
      json['devices'].forEach((e) {
        devices!.add(Device.fromJson(e));
      });
    }
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }
}
