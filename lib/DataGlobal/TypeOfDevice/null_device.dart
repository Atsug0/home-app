import 'package:flutter_app/DataGlobal/device.dart';

class NullDevice extends Device {
  NullDevice();

  @override
  String getState() {
    return "";
  }

  @override
  int getValue() {
    return (0);
  }

  @override
  String getMode() {
    return "";
  }

  @override
  String getName() {
    return "";
  }

  @override
  String getProd() {
    return "";
  }

  @override
  void setValue(int value) {}

  @override
  void setMode(String mode) {}
}
