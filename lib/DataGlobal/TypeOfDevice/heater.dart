import 'package:flutter_app/DataGlobal/device.dart';

class Heater extends Device {
  int id;
  String deviceName;
  String mode;
  String productType;
  int temperature;

  Heater(
      this.id,
      this.deviceName,
      this.mode,
      this.productType,
      this.temperature
  );

  @override
  String getName() {
    return deviceName;
  }

  @override
  String getState() {
    if (mode == "OFF") return ("off");
    return ("on at $temperature°C");
  }

  @override
  String getProd() {
    return productType;
  }

  @override
  String getMode() {
    return mode;
  }

  @override
  int getValue() {
    return temperature;
  }

  @override
  void setValue(int value) {
    temperature = value;
  }

  @override
  void setMode(String mode) {
    this.mode = mode;
  }
}
