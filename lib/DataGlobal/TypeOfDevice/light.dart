import '../device.dart';

class Light extends Device {
  int id;
  String deviceName;
  int intensity;
  String mode;
  String productType;

  Light(
    this.id,
    this.deviceName, 
    this.intensity, 
    this.mode, 
    this.productType
  );

  @override
  String getState() {
    if (intensity == 0 || mode == "OFF") {
      return ("off");
    }
    if (intensity == 100 && mode == "ON") {
      return ("on");
    }
    if (intensity > 0 && mode == "ON") {
      return ("on at $intensity%");
    }
    return "";
  }

  @override
  String getProd() {
    return productType;
  }

  @override
  int getValue() {
    return intensity;
  }

  @override
  String getMode() {
    return mode;
  }

  @override
  String getName() {
    return deviceName;
  }

  @override
  void setMode(String mode) {
    this.mode = mode;
  }

  @override
  void setValue(int value) {
    intensity = value;
  }
}
