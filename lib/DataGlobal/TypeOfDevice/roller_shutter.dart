import 'package:flutter_app/DataGlobal/device.dart';

class RollerShutter extends Device {
  int id;
  String deviceName;
  String productType;
  int position;

  RollerShutter(
    this.id, 
    this.deviceName, 
    this.productType, 
    this.position
  );

  @override
  String getState() {
    if (position == 0) {
      return ("closed");
    }
    if (position == 100) {
      return ("opened");
    }
    return ("opened at $position%");
  }

  @override
  int getValue() {
    return position;
  }

  @override
  String getProd() {
    return productType;
  }

  @override
  String getMode() {
    return "";
  }

  @override
  String getName() {
    return deviceName;
  }

  @override
  void setMode(String mode) {
  }

  @override
  void setValue(int value) {
    position = value;
  }
}
