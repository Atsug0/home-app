// ignore: file_names
import 'package:flutter_app/DataGlobal/TypeOfDevice/heater.dart';
import 'package:flutter_app/DataGlobal/TypeOfDevice/light.dart';
import 'package:flutter_app/DataGlobal/TypeOfDevice/null_device.dart';
import 'package:flutter_app/DataGlobal/TypeOfDevice/roller_shutter.dart';

abstract class Device {
  Device();

  static fromJson(Map<String, dynamic> json) {
    switch (json["productType"]) {
      case "Light":
        return Light(json["id"] ?? 0.0, json["deviceName"] ?? "none",
            json["intensity"] ?? 0.0, json["mode"] ?? "off", "Light");
      case "RollerShutter":
        return RollerShutter(json["id"] ?? 0.0, json["deviceName"] ?? "none",
            "RollerShutter", json["position"] ?? 0.0);
      case "Heater":
        return Heater(json["id"] ?? 0.0, json["deviceName"] ?? "none",
            json["mode"] ?? "off", "Heater", json["temperature"] ?? 0.0);
      default:
    }
    return NullDevice();
  }

  Map<String, dynamic> toJson() => {};
  String getState();
  String getMode();
  int getValue();
  String getName();
  String getProd();

  void setValue(int value);
  void setMode(String mode);
}
