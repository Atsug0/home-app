import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'GetJson/get_parse.dart';
import 'Device_bloc/device_bloc.dart';
import 'Pages/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'FlutterApp',
        home: MultiBlocProvider(providers: [
          BlocProvider(
            create: (context) => DeviceBloc(GetParsed()),
          )
        ], child: const Home(),
        )
      );
  }
}
