import 'package:equatable/equatable.dart';

abstract class Event extends Equatable {}

class DeviceEvent extends Event {
  @override
  List<Object> get props => [];
}

