import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_app/GetJson/get_parse.dart';
import 'package:flutter_app/Device_bloc/exported_bloc_device.dart';
import 'package:flutter_app/Device_state/exported_state.dart';
import 'device_event.dart';

class DeviceBloc extends Bloc<Event, DeviceState> {
  final GetParse data;

  DeviceBloc(this.data) : super(InitialState()) {
    on<DeviceEvent>((event, emit) async {
      emit(InitialState());
      try {
        final devices = await data.getDevice();
        emit(DataState(deviceList: devices));
      } catch (e) {
        print(e.toString());
        emit(ErrorState(error: e.toString()));
      }
    });
  }
}
