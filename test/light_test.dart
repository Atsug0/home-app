import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_app/DataGlobal/TypeOfDevice/light.dart';

void main() {
  late Light testing;

  setUp(() {
    testing = Light(1, "test", 26, "ON", "Light");
  });

  group('Light', () {
    test('see if a Light value is change', () {
      testing.setValue(33);
      expect(testing.getValue(), 33);
    });

    test('see if mode is change', () {
      testing.setMode("OFF");
      expect(testing.getMode(), "OFF");
    });

    test('see if state is correct', () {
      testing.setValue(26);
      testing.setMode("ON");
      expect(testing.getState(), "on at 26%");
    });

    test('see if getter product is correct', () {
      expect(testing.getProd(), "Light");
    });

    test('see if getter Value is correct', () {
      final Light test5 = Light(1, "test5", 26, "ON", "Light");
      expect(test5.getValue(), 26);
    });

    test('see if getter mode is correct', () {
      expect(testing.getMode(), "ON");
    });

    test('see if getter name is correct', () {
      expect(testing.getName(), "test");
    });
  });
}
