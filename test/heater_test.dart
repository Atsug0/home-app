import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_app/DataGlobal/TypeOfDevice/heater.dart';

void main() {
  late Heater testing;

  setUp(() {
    testing = Heater(1, "test", "ON", "Heater", 26);
  });

  group('Heater', () {
    test('see if a heater value is change', () {
      testing.setValue(33);
      expect(testing.getValue(), 33);
    });

    test('see if mode is change', () {
      testing.setMode("OFF");

      expect(testing.getMode(), "OFF");
    });

    test('see if state is correct', () {
      testing.setMode("ON");
      testing.setValue(26);
      expect(testing.getState(), "on at 26°C");
    });

    test('see if getter product is correct', () {

      expect(testing.getProd(), "Heater");
    });

    test('see if getter Value is correct', () {

      expect(testing.getValue(), 26);
    });

    test('see if getter mode is correct', () {

      expect(testing.getMode(), "ON");
    });

    test('see if getter name is correct', () {

      expect(testing.getName(), "test");
    });
  });
}
