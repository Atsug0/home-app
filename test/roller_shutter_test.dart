import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_app/DataGlobal/TypeOfDevice/roller_shutter.dart';

void main() {
  late RollerShutter testing;

  setUp(() {
    testing = RollerShutter(1, "test", "RollerShutter", 26);
  });
  group('RollerShutter', () {
    test('see if a RollerShutter value is change', () {
      testing.setValue(33);
      expect(testing.getValue(), 33);
    });

    test('see if mode is change', () {
      testing.setMode("OFF");
      expect(testing.getMode(), "");
    });

    test('see if state is correct', () {
      testing.setValue(26);
      expect(testing.getState(), "opened at 26%");
    });

    test('see if getter product is correct', () {
      expect(testing.getProd(), "RollerShutter");
    });

    test('see if getter Value is correct', () {
      expect(testing.getValue(), 26);
    });

    test('see if getter mode is correct', () {
      expect(testing.getMode(), "");
    });

    test('see if getter name is correct', () {
      expect(testing.getName(), "test");
    });
  });
}
