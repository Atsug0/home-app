import 'package:flutter_app/DataGlobal/device.dart';
import 'package:flutter_app/GetJson/get_parse.dart';
import 'package:flutter_test/flutter_test.dart';
//it testing the static method Json in device with the call of getDevice and its testing if the list is not empty
void main() {
  late GetParsed data;

  setUp(() {
    data = GetParsed();
  });
  test('see if data is created', () {
    Future<List<Device>> recu;

    recu = data.getDevice();
    expect(recu, isNotNull);
  });
}
