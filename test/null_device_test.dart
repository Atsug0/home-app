import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_app/DataGlobal/TypeOfDevice/null_device.dart';

void main() {
  late NullDevice testing;

  setUp(() {
    testing = NullDevice();
  });
  group('NullDevice', () {
    test('see if a NullDevice value is change', () {
      testing.setValue(33);
      expect(testing.getValue(), 0);
    });

    test('see if mode is change', () {
      testing.setMode("OFF");
      expect(testing.getMode(), "");
    });

    test('see if state is correct', () {
      expect(testing.getState(), "");
    });

    test('see if getter product is correct', () {
      expect(testing.getProd(), "");
    });

    test('see if getter Value is correct', () {
      expect(testing.getValue(), 0);
    });

    test('see if getter mode is correct', () {
      expect(testing.getMode(), "");
    });

    test('see if getter name is correct', () {
      expect(testing.getName(), "");
    });
  });
}
